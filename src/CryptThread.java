import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 25.10.2017.
 */
public class CryptThread extends Thread {

    private static final int BUFFER_SIZE = 8192;

    private Cipher cipher;
    private FileInputStream fileInputStream;
    private FileOutputStream fileOutputStream;


    public CryptThread(Cipher cipher, FileOutputStream fileOutputStream, FileInputStream fileInputStream, FileWriter logWriter, String PROCESS) {
        this.cipher = cipher;
        this.fileOutputStream = fileOutputStream;
        this.fileInputStream = fileInputStream;
    }

    @Override
    public void run() {

        try {
            int byteCount;
            byte buffer[] = new byte[BUFFER_SIZE];
            byte[] encrypted;

            while ((byteCount = fileInputStream.read(buffer)) > 0) {
                encrypted = cipher.update(buffer, 0, byteCount);
                if (encrypted != null) {
                    fileOutputStream.write(encrypted);
                }
            }
            encrypted = cipher.doFinal();
            if (encrypted != null) {
                fileOutputStream.write(encrypted);
            }

        } catch (BadPaddingException e) {
            System.out.println("Invalid Padding. " + CryptUtil.PKCS5Padding);
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            System.out.println("Illegal Block Size: " + e.getMessage());
            System.exit(1);
        }
    }
}
