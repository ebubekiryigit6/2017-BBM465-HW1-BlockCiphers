import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.channels.IllegalBlockingModeException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 20.10.2017.
 */

public class CryptUtil {

    public static final String AES = "AES";
    public static final String DES = "DES";

    public static final String CTR = "CTR";
    public static final String OFB = "OFB";
    public static final String CBC = "CBC";

    public static final String NoPadding = "NoPadding";
    public static final String PKCS5Padding = "PKCS5Padding";

    private String inputFilePath;
    private String outputFilePath;
    private String ENCRYPTION_ALGORITHM = AES;
    private String ENCRYPTION_MODE = CBC;
    private String PADDING_METHOD = PKCS5Padding;
    private String KEY;
    private String INIT_VECTOR;
    private int THREAD_COUNT = 1;
    private String PROCESS = "-e";

    private static final String LOG_FILE = "run.log";
    private FileWriter logWriter;

    private static final String AES_TEST_KEY = "1111111111111111";
    private static final String DES_TEST_KEY = "11111111";
    private static final String AES_TEST_INIT_VECTOR = "a1a2a3a4a5a6a7a8";
    private static final String DES_TEST_INIT_VECTOR = "a1a2a3a4";
    private boolean DEBUG_MODE = false;

    public CryptUtil setDebugMode(boolean debugMode) {
        this.DEBUG_MODE = debugMode;
        return this;
    }

    /**
     * creates a completely cipher instance
     */
    public void create() {
        if (DEBUG_MODE) {
            if (ENCRYPTION_ALGORITHM.equals(AES)) {
                KEY = AES_TEST_KEY;
                INIT_VECTOR = AES_TEST_INIT_VECTOR;
            } else if (ENCRYPTION_ALGORITHM.equals(DES)) {
                KEY = DES_TEST_KEY;
                INIT_VECTOR = DES_TEST_INIT_VECTOR;
            }
        }

        try {
            File logFile = new File(LOG_FILE);
            if (!logFile.exists()) {
                logFile.createNewFile();
            }
            logWriter = new FileWriter(LOG_FILE, true);
        } catch (IOException e) {
            System.out.println("Error in open " + LOG_FILE + " file.");
        }
    }

    public CryptUtil setProcess(String process) {
        if (process.equalsIgnoreCase(CommandArgumentParser.encryption_parameter)) {
            PROCESS = CommandArgumentParser.encryption_parameter;
        } else if (process.equalsIgnoreCase(CommandArgumentParser.decryption_parameter)) {
            PROCESS = CommandArgumentParser.decryption_parameter;
        } else {
            System.out.println("Invalid encryption or decryption parameter.");
        }
        return this;
    }

    public CryptUtil setKey(String key) {
        this.KEY = key;
        return this;
    }

    public CryptUtil setIV(String IV) {
        this.INIT_VECTOR = IV;
        return this;
    }

    public CryptUtil setThreadCount(int threadCount) {
        this.THREAD_COUNT = threadCount;
        return this;
    }

    public CryptUtil setPadding(String padding) {
        if (padding.equalsIgnoreCase(NoPadding)) {
            this.PADDING_METHOD = NoPadding;
        } else if (padding.equalsIgnoreCase(PKCS5Padding)) {
            this.PADDING_METHOD = PKCS5Padding;
        } else {
            System.out.println("Invalid padding method.");
            System.exit(1);
        }
        return this;
    }

    public CryptUtil setEncryptionAlgorithm(String algorithm) {
        if (algorithm.equalsIgnoreCase(AES)) {
            this.ENCRYPTION_ALGORITHM = AES;
        } else if (algorithm.equalsIgnoreCase(DES)) {
            this.ENCRYPTION_ALGORITHM = DES;
        } else {
            System.out.println("Invalid encryption algorithm.");
            System.exit(1);
        }
        return this;
    }

    public CryptUtil setBlockingMode(String blockingMode) {
        if (blockingMode.equalsIgnoreCase(CTR)) {
            this.ENCRYPTION_MODE = CTR;
        } else if (blockingMode.equals(CBC)) {
            this.ENCRYPTION_MODE = CBC;
        } else if (blockingMode.equalsIgnoreCase(OFB)) {
            this.ENCRYPTION_MODE = OFB;
        } else {
            System.out.println("Invalid blocking mode.");
            System.exit(1);
        }
        return this;
    }

    public CryptUtil setFilePaths(String inputFilePath, String outputFilePath) {
        setInputFilePath(inputFilePath);
        setOutputFilePath(outputFilePath);
        return this;
    }

    private void setInputFilePath(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    private void setOutputFilePath(String outputFilePath) {
        this.outputFilePath = outputFilePath;
    }


    public void encrypt() {
        try {
            // create an digest and get KEY bytes
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(KEY.getBytes("UTF-8"));
            byte[] keyBytes = new byte[KEY.length()];
            System.arraycopy(digest.digest(), 0, keyBytes, 0, keyBytes.length);

            // create key and IV specs, init cipher instance
            SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, ENCRYPTION_ALGORITHM);
            Cipher cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM + "/" + ENCRYPTION_MODE + "/" + PADDING_METHOD);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(INIT_VECTOR.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

            File inputFile = new File(inputFilePath);
            File outputFile = new File(new File(inputFile.getAbsolutePath()).getParent(), outputFilePath);
            outputFile.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            FileInputStream inputStream = new FileInputStream(inputFile);

            // Effective buffer size for block ciphers
            int BUFFER_SIZE = 8192;
            int byteCount;
            byte buffer[] = new byte[BUFFER_SIZE];
            byte[] encrypted;

            // start timer
            StopWatch stopwatch = new StopWatch();

            // encrypt file part by part
            while ((byteCount = inputStream.read(buffer)) > 0) {
                encrypted = cipher.update(buffer, 0, byteCount);
                if (encrypted != null) {
                    fileOutputStream.write(encrypted);
                }
            }
            // everything is OK?
            encrypted = cipher.doFinal();
            if (encrypted != null) {
                fileOutputStream.write(encrypted);
            }

            // get exec time and write all processes to log file
            long time = stopwatch.elapsedTime();
            writeLogFile(time, inputFile, outputFile);

        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("Invalid Algorithm Parameter: " + e.getMessage());
            System.exit(1);
        } catch (IllegalBlockingModeException e) {
            System.out.println("Illegal Blocking Mode: " + e.getMessage());
            System.exit(1);
        } catch (NoSuchPaddingException e) {
            System.out.println("Invalid Padding: " + e.getMessage());
            System.exit(1);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Invalid Algorithm: " + ENCRYPTION_ALGORITHM);
            System.exit(1);
        } catch (UnsupportedEncodingException e) {
            System.out.println("Unsupported encoding: UTF-8");
            System.exit(1);
        } catch (IllegalBlockSizeException e) {
            System.out.println("Invalid Block Size: " + e.getMessage());
            System.exit(1);
        } catch (InvalidKeyException e) {
            System.out.println("Invalid key: " + KEY);
            System.exit(1);
        } catch (BadPaddingException e) {
            System.out.println("Bad Padding: " + PADDING_METHOD);
            System.exit(1);
        } catch (Exception e) {
            System.out.println("Unexpected error: " + e.getMessage());
            System.exit(1);
        }
    }

    public void decrypt() {
        try {
            // create an digest and get KEY bytes
            byte[] keyBytes = new byte[KEY.length()];
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(KEY.getBytes());
            System.arraycopy(md.digest(), 0, keyBytes, 0, keyBytes.length);
            SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, ENCRYPTION_ALGORITHM);

            // create key and IV specs, init cipher instance
            Cipher cipherDecrypt = Cipher.getInstance(ENCRYPTION_ALGORITHM + "/" + ENCRYPTION_MODE + "/" + PADDING_METHOD);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(INIT_VECTOR.getBytes());
            cipherDecrypt.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

            File inputFile = new File(inputFilePath);
            File outputFile = new File(new File(inputFile.getAbsolutePath()).getParent(), outputFilePath);
            outputFile.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            FileInputStream inputStream = new FileInputStream(inputFile);

            // Effective buffer size for block ciphers
            int BUFFER_SIZE = 8192;
            byte[] buffer = new byte[BUFFER_SIZE];
            byte[] decrypted;
            int byteCount;

            // start timer
            StopWatch stopwatch = new StopWatch();

            // decrypt file part by part
            while ((byteCount = inputStream.read(buffer)) > 0) {
                decrypted = cipherDecrypt.update(buffer, 0, byteCount);
                if (decrypted != null) {
                    fileOutputStream.write(decrypted);
                }
            }

            // everything is OK?
            decrypted = cipherDecrypt.doFinal();
            if (decrypted != null) {
                fileOutputStream.write(decrypted);
            }

            // get exec time and write all processes to log file
            long time = stopwatch.elapsedTime();
            writeLogFile(time, inputFile, outputFile);

        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("Invalid Algorithm Parameter exception: " + e.getMessage());
            System.exit(1);
        } catch (IllegalBlockingModeException e) {
            System.out.println("Illegal Blocking Mode: " + e.getMessage());
            System.exit(1);
        } catch (NoSuchPaddingException e) {
            System.out.println("Invalid Padding:" + e.getMessage());
            System.exit(1);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Invalid Algorithm: " + ENCRYPTION_ALGORITHM);
            System.exit(1);
        } catch (IllegalBlockSizeException e) {
            System.out.println("Invalid Block Size: " + e.getMessage());
            System.exit(1);
        } catch (InvalidKeyException e) {
            System.out.println("Invalid key: " + KEY);
            System.exit(1);
        } catch (BadPaddingException e) {
            System.out.println("Bad Padding: " + PADDING_METHOD);
            System.exit(1);
        } catch (Exception e) {
            System.out.println("Unexpected error: " + e.getMessage());
            System.exit(1);
        }
    }

    private void writeLogFile(long execTime, File inputFile, File outputFile) throws IOException {
        String encryptionDecryption = "enc";
        if (PROCESS.equalsIgnoreCase("-e")) {
            encryptionDecryption = "enc";
        } else if (PROCESS.equalsIgnoreCase("-d")) {
            encryptionDecryption = "dec";
        }

        if (logWriter == null) {
            logWriter = new FileWriter(LOG_FILE, true);
        }
        logWriter.write(
                inputFile.getAbsolutePath()
                        + " "
                        + outputFile.getAbsolutePath()
                        + " "
                        + encryptionDecryption
                        + " "
                        + ENCRYPTION_ALGORITHM
                        + " "
                        + ENCRYPTION_MODE
                        + " "
                        + execTime
                        + "\n");
        logWriter.close();
    }
}
