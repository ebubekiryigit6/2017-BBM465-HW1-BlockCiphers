import java.io.BufferedReader;
import java.io.FileReader;

/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 24.10.2017.
 */
public class CommandArgumentParser {

    public static final String encryption_parameter = "-e";
    public static final String decryption_parameter = "-d";
    public static final String thread_parameter = "-p";
    public static final String input_file_parameter = "-i";
    public static final String output_file_parameter = "-o";

    private String ENCRYPTION_ALGORITHM = CryptUtil.AES;
    private String ENCRYPTION_MODE = CryptUtil.CBC;
    private String KEY = "a1a2a3a4";
    private String IV = "s1s2s3s4";
    private String PROCESS = encryption_parameter;
    private String inputFilePath = "";
    private String outputFilePath = "";
    private String keyFilePath = "";
    private boolean isMultithreadingEnabled = false;
    private int thread_count = 1;

    public CommandArgumentParser(String[] argumentLine) {
        // -p argument is available
        if (argumentLine.length == 10) {
            createParser(argumentLine, true);
            // -p argument is not available
        } else if (argumentLine.length == 8) {
            createParser(argumentLine, false);
        } else {
            System.out.println("Wrong argument format.");
            printUsage();
            System.exit(1);
        }
    }

    /**
     * parser gets shell line and parses all shell line
     *
     * @param argumentLine     shell line
     * @param isMultithreading is -p available
     */
    private void createParser(String[] argumentLine, boolean isMultithreading) {
        int COUNT = 1;
        if (isMultithreading) {
            COUNT = 3;
            if (argumentLine[1].equalsIgnoreCase(thread_parameter)) {
                try {
                    this.thread_count = Integer.parseInt(argumentLine[2]);
                    this.isMultithreadingEnabled = true;
                } catch (Exception e) {
                    System.out.println("Invalid thread parameter.");
                    printUsage();
                    System.exit(1);
                }
            } else {
                System.out.println("Invalid thread parameter.");
                printUsage();
                System.exit(1);
            }
        }

        // get process choice
        if (argumentLine[0].equalsIgnoreCase(encryption_parameter)) {
            this.PROCESS = encryption_parameter;
        } else if (argumentLine[0].equalsIgnoreCase(decryption_parameter)) {
            this.PROCESS = decryption_parameter;
        } else {
            System.out.println("Invalid encryption or decryption parameter.");
            printUsage();
            System.exit(1);
        }

        // get input file path
        if (argumentLine[COUNT].equalsIgnoreCase(input_file_parameter)) {
            this.inputFilePath = argumentLine[COUNT + 1];
        } else {
            System.out.println("Invalid input file parameter.");
            printUsage();
            System.exit(1);
        }

        // get output file name
        if (argumentLine[COUNT + 2].equalsIgnoreCase(output_file_parameter)) {
            this.outputFilePath = argumentLine[COUNT + 3];
        } else {
            System.out.println("Invalid output file parameter.");
            printUsage();
            System.exit(1);
        }

        // get encryption algorithm
        if (argumentLine[COUNT + 4].equalsIgnoreCase(CryptUtil.AES)) {
            this.ENCRYPTION_ALGORITHM = CryptUtil.AES;
        } else if (argumentLine[COUNT + 4].equalsIgnoreCase(CryptUtil.DES)) {
            this.ENCRYPTION_ALGORITHM = CryptUtil.DES;
        } else {
            System.out.println("Invalid algorithm parameter.");
            printUsage();
            System.exit(1);
        }

        // get blocking mode
        if (argumentLine[COUNT + 5].equalsIgnoreCase(CryptUtil.CBC)) {
            this.ENCRYPTION_MODE = CryptUtil.CBC;
        } else if (argumentLine[COUNT + 5].equalsIgnoreCase(CryptUtil.OFB)) {
            this.ENCRYPTION_MODE = CryptUtil.OFB;
        } else if (argumentLine[COUNT + 5].equalsIgnoreCase(CryptUtil.CTR)) {
            this.ENCRYPTION_MODE = CryptUtil.CTR;
        } else {
            System.out.println("Invalid encryption mode parameter.");
            printUsage();
            System.exit(1);
        }

        // get keystore file
        this.keyFilePath = argumentLine[COUNT + 6];
        readKeyFile();
    }

    /**
     * reads keystore file and get IV and KEY strings
     */
    private void readKeyFile() {
        try {
            String line;
            BufferedReader bufferedReader = new BufferedReader(new FileReader(this.keyFilePath));
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (!line.isEmpty()) {
                    break;
                }
            }

            try {
                String[] keyAndIV = line.split("-");
                this.IV = keyAndIV[0].trim();
                this.KEY = keyAndIV[1].trim();
            } catch (Exception e) {
                System.out.println("IV - KEY parse ERROR, Wrong format. It must be <IV - KEY>");
                printUsage();
                System.exit(1);
            }
        } catch (Exception e) {
            System.out.println("Reading KEY file ERROR.");
            printUsage();
            System.exit(1);
        }
    }

    private void printUsage() {
        System.out.println("Usage: FileCrypt <-e/-d> [-p #] -i <infile> -o <outfile> <enc_alg> <mode> <key_file>");
    }

    public String getKEY() {
        return KEY;
    }

    public String getIV() {
        return IV;
    }

    public int getThread_count() {
        return thread_count;
    }

    public String getENCRYPTION_ALGORITHM() {
        return ENCRYPTION_ALGORITHM;
    }

    public String getENCRYPTION_MODE() {
        return ENCRYPTION_MODE;
    }

    public String getInputFilePath() {
        return inputFilePath;
    }

    public String getOutputFilePath() {
        return outputFilePath;
    }

    public String getPROCESS() {
        return PROCESS;
    }

    public boolean isMultithreadingEnabled() {
        return isMultithreadingEnabled;
    }
}
