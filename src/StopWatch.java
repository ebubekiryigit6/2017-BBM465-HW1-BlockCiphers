/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 19.10.2017.
 */

public class StopWatch {

    /*
          Usage:  Create an StopWatch object and do your work. And call elapsedTime() function.

          StopWatch stopWatch = new StopWatch();
          // do something
          double time = stopWatch.elapsedTime();
     */

    private final long start;

    public StopWatch() {
        start = System.currentTimeMillis();
    }

    public long elapsedTime() {
        long now = System.currentTimeMillis();
        return (now - start);
    }


}


