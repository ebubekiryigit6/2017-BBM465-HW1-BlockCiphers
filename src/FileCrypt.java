/**
 * </>
 * Created by Ebubekir YIGIT and Huseyin KAYAYURT on 19.10.2017.
 */
public class FileCrypt {

    public static void main(String[] args) {

        // parse the command shell arguments
        CommandArgumentParser parser = new CommandArgumentParser(args);

        // create Cipher instance and init algorithms
        CryptUtil cryptUtil = new CryptUtil();
        cryptUtil.setEncryptionAlgorithm(parser.getENCRYPTION_ALGORITHM())
                .setBlockingMode(parser.getENCRYPTION_MODE())
                .setPadding(CryptUtil.PKCS5Padding)
                .setProcess(parser.getPROCESS())
                .setThreadCount(parser.getThread_count())
                .setKey(parser.getKEY())
                .setIV(parser.getIV())
                .setFilePaths(parser.getInputFilePath(), parser.getOutputFilePath())
                .setDebugMode(false)
                .create();


        // encrypting or decrypting process
        String process = parser.getPROCESS();
        if (process.equalsIgnoreCase(CommandArgumentParser.encryption_parameter)) {
            cryptUtil.encrypt();
        } else if (process.equalsIgnoreCase(CommandArgumentParser.decryption_parameter)) {
            cryptUtil.decrypt();
        } else {
            System.out.println("Invalid encryption/decryption type. You must use -e or -d");
            System.exit(1);
        }
    }
}
